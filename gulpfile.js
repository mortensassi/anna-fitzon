let gulp = require('gulp'),
    fs = require('fs'),
    requireDir = require('require-dir'),
    runSequence = require('run-sequence'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    });


let config = JSON.parse(fs.readFileSync('./config.json'));

requireDir('./gulp-tasks');

gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras'], () => {
  return gulp.src('app/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('build:template', ['lint', 'templates', 'images', 'fonts', 'extras'], () => {
  return gulp.src('wp/**/*').pipe($.size({title: 'build', gzip: true}));
});

gulp.task('default', () => {
  runSequence(['clean', 'wiredep'], 'build');
});
