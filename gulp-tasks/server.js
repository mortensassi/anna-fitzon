let gulp = require('gulp'),
    fs = require('fs'),
    runSequence = require('run-sequence'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    }),
    reload = $.browserSync.reload;

let config = JSON.parse(fs.readFileSync('./config.json'));

gulp.task('serve', function () {
  runSequence(['clean', 'wiredep'], ['views:build', 'styles:build', 'scripts:build', 'fonts', 'images'], () => {
    $.browserSync.init({
      notify: false,
      port: 4200,
      server: {
        baseDir: '.tmp',
        routes: {
          '/bower_components': 'bower_components'
        }
      }
    });

    gulp.watch([
      config.views.src + '**/*.twig',
      config.images.src + '**/*',
      config.fonts.tmp + 'fonts/**/*'
    ]).on('change', reload);


    gulp.watch(config.views.src + '**/*.twig', ['views:build']);
    gulp.watch(config.data.src + '**/*.json', ['views:templates']);
    gulp.watch(config.styles.src + '**/*.scss', ['styles:build']);
    gulp.watch(config.scripts.src + '**/*.js', ['lint', 'scripts:build']);
    gulp.watch(config.fonts.src + '**/*', ['fonts']);
    gulp.watch(config.images.src + '**/*', ['images']);
    gulp.watch('bower.json', ['wiredep', 'fonts']);
  });
});

gulp.task('serve:dist', ['default'], () => {
  $.browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ['app']
    }
  });
});
