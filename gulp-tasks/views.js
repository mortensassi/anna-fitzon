let gulp = require('gulp'),
    fs = require('fs'),
    path = require('path'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    }),
    reload = $.browserSync.reload;

let config = JSON.parse(fs.readFileSync('./config.json'));

// Compile Twig Templates
gulp.task('views:build', ['views:templates'], () => {
  return gulp.src(config.views.src + '*.twig')
      .pipe($.cached(config.views.tmp))
      .pipe($.data(function() {
        return JSON.parse(fs.readFileSync('src/data/dummy.json'))
      })) // load data from base json file
      .pipe($.twig())
      .pipe(gulp.dest(config.views.tmp))
      .pipe(reload({stream: true}));
});

gulp.task('views:templates', (cb) => {
  let outputFilename = 'dummy.json';

  gulp.src('src/data/**/*.json')
      .pipe($.mergeJson({
        fileName : outputFilename
      }))
      .pipe(gulp.dest(config.data.src))
      .on('end', function () {
        cb();
      })
});
