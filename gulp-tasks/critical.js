let gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    }),
    fs = require('fs'),
    gutil = require('gulp-util'),
    critical = require('critical').stream;


let config = JSON.parse(fs.readFileSync('./config.json'));

gulp.task('critical', ['build'], function () {
  return gulp.src(config.views.dest + '/*.html')
      .pipe(critical({
        base: config.views.dest,
        inline: true,
        minify: true,
        css: [config.styles.dest + '/vendor.css', config.styles.dest + '//main.css']
      }))
      .on('error', function(err) { gutil.log(gutil.colors.red(err.message)); })
      .pipe(gulp.dest(config.views.dest));
});
