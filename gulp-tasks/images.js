let gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    }),
    fs = require('fs'),
    reload = $.browserSync.reload;

let config = JSON.parse(fs.readFileSync('./config.json'));

gulp.task('images', function() {
  return gulp.src(config.images.src + '**/*.{jpg,jpeg,png,svg}')
      .pipe($.changed(config.images.dest))
      .pipe($.rename({dirname: ''}))
      .pipe($.imagemin())
      .pipe(gulp.dest(config.images.dest))
      .pipe(gulp.dest(config.images.tmp))
      .pipe(gulp.dest(config.images.wp))
      .pipe(reload({stream: true}));
});
