let gulp = require('gulp'),
    fs = require('fs'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    });

let config = JSON.parse(fs.readFileSync('./config.json'));

gulp.task('html', ['views:build', 'styles:build', 'scripts:build'], () => {
  return gulp.src(['.tmp/*.html', 'src/*.html'])
      .pipe($.useref({searchPath: ['.tmp','src', '.']}))
      .pipe($.if('*.js', $.uglify()))
      .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
      .pipe($.if('*.html', $.htmlmin({collapseWhitespace: true})))
      .pipe(gulp.dest(config.views.dest))
});

gulp.task('templates', ['views:templates', 'styles:build', 'scripts:build'], () => {
  return gulp.src('src/**/*.twig')
      .pipe($.useref({searchPath: ['src', '.']}))
      .pipe($.if('*.js', $.uglify()))
      .pipe($.if('*.css', $.cssnano({safe: true, autoprefixer: false})))
      .pipe(gulp.dest(config.views.wp));
});
