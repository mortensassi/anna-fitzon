let gulp = require('gulp'),
    fs = require('fs'),
    wiredep = require('wiredep').stream,
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    });

let config = JSON.parse(fs.readFileSync('./config.json'));

gulp.task('wiredep', () => {
  gulp.src(config.styles.src + '*.scss')
      .pipe($.filter(file => file.stat && file.stat.size))
      .pipe(wiredep({
        ignorePath: /^(\.\.\/)+/
      }))
      .pipe(gulp.dest(config.styles.src));

  gulp.src(config.views.src + 'layouts/base.twig')
      .pipe(wiredep({
        exclude: ['bootstrap-sass'],
        ignorePath: /^(\.\.\/)*\.\./
      }))
      .pipe(gulp.dest(config.views.src + 'layouts'));
});
