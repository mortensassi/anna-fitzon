let gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    }),
    fs = require('fs'),
    reload = $.browserSync.reload;


let config = JSON.parse(fs.readFileSync('./config.json'));

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
      .concat(config.fonts.src + '**/*'))
      .pipe(gulp.dest(config.fonts.dest))
      .pipe(gulp.dest(config.fonts.tmp))
      .pipe(gulp.dest(config.fonts.wp));
});
