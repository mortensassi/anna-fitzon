let gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    }),
    del = require('del'),
    fs = require('fs'),
    reload = $.browserSync.reload;

let config = JSON.parse(fs.readFileSync('./config.json'));

/**
 * Cleanup
 */

gulp.task('clean', del.bind(null, [config.views.tmp, config.views.dest, config.views.src + 'dummy.json']));


/**
 * Lint
 */

function lint(files, options) {
  return gulp.src(files)
      .pipe($.eslint({fix: true}))
      .pipe(reload({stream: true, once: true}))
      .pipe($.eslint.format())
      .pipe($.if(!$.browserSync.active, $.eslint.failAfterError()));
}
gulp.task('lint', () => {
  return lint(config.scripts.src + '**/*.js')
      .pipe(gulp.dest(config.scripts.src));
});
gulp.task('lint:test', () => {
  return lint('test/spec/**/*.js')
      .pipe(gulp.dest('test/spec'));
});

/**
 * Extras
 */

gulp.task('extras', () => {
  return gulp.src([
    'src/*',
    '!src/*.html',
    '!src/*.twig',
    '!src/{data, data/**}',
    '!src/{views, views/**}'
  ], {
    dot: true
  })
      .pipe(gulp.dest(config.views.dest));
});
