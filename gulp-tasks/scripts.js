let gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    }),
    fs = require('fs'),
    reload = $.browserSync.reload;

let config = JSON.parse(fs.readFileSync('./config.json'));

/**
 * Build one single .js file
 * Files can set in the config.json
 */
gulp.task('scripts:build', function () {
    return gulp.src(config.scripts.src + '*.js')
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.babel({ presets: ['babel-preset-es2015'].map(require.resolve) }))
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest(config.scripts.tmp))
        .pipe(reload({stream: true}));
});
