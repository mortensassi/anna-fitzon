let gulp = require('gulp'),
    $ = require('gulp-load-plugins')({
      pattern: '*' // load not only plugins which are prefixed "gulp-"
    }),
    fs = require('fs'),
    reload = $.browserSync.reload;


let config = JSON.parse(fs.readFileSync('./config.json'));

gulp.task('styles:build', function () {
    return gulp.src(config.styles.src + '*.scss')
        .pipe($.plumber())
        .pipe($.sourcemaps.init())
        .pipe($.sass.sync({
          outputStyle: 'expanded',
          precision: 10,
          includePaths: ['.']
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'ie >= 9']}))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(config.styles.tmp))
        .pipe(reload({stream: true}));
});
