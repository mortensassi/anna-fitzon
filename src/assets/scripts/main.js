/**
 * @type {FontFaceObserver}
 */

let font = new FontFaceObserver('space_monoregular');

font.load().then(function () {
  document.body.classList.add('font-loaded');
  $('.project').addClass('project--show')
});


/**
 * On Page Load
 */

$(document).ready(function () {

  // Trigger
  let $menuTrigger = $('[aria-label="site"] button'),
      $menuTarget = $('.menu');

  trigger($menuTrigger, $menuTarget);

  $('.project').each(function () {
    trigger($(this).find('.project__header button'), $(this).find('.project__info'))
  });

  trigger($('#aboutTrigger'), $('.profile'));


  // Slick
  $('.slider').slick({
    arrows: false,
    dots: true,
    mobileFirst: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          fade: true,
          arrows: true
        }
      }
    ]
  });


  // Waypoint
  $('.project:nth-child(2)').attr('id', 'waypoint');
  let waypoint = new Waypoint({
    element: document.getElementById('waypoint'),
    handler: function (direction) {
      $('.back-to-top').toggleClass('back-to-top--visible');
    }
  });

  $('.back-to-top').on('click', function () {
    $('body,html').animate({
      scrollTop: 0
    }, 800);
    return false;
  });

});
