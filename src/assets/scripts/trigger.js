function trigger(trigger, target) {
  let $trigger = trigger,
      $target = target;

  $trigger.attr('aria-expanded', 'false');
  $target.attr('aria-expanded', 'false');

  $trigger.on('click', function () {
    if ($(this).attr('aria-expanded') === 'false') {
      $(this).add($target).attr('aria-expanded', 'true');

    } else {
      $(this).add($target).attr('aria-expanded', 'false');
    }
  })
}
